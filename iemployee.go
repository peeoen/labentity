package labentity

type IEmployee interface {
	GetFullName() string
	GetDepartment() string
	SearchEmployee()
	AddEmployee()
}
