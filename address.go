package labentity

import (
	"encoding/json"
	"fmt"
)

type Address struct {
	Address1 string
	Address2 string
	Province string
	Postcode int
	Country  string
}

func (a Address) String() string {
	if rawJson, err := json.MarshalIndent(&a, " ", " "); err != nil {
		return ""
	} else {
		return string(rawJson)
	}
}

func (a *Address) GetAddress() string {
	return fmt.Sprintf("%s %s %s %s",
		a.Address1,
		a.Address2,
		a.Province,
		a.Country)
}

func (a *Address) GetPostCode() string {
	return fmt.Sprintf("%s",
		a.Postcode)
}
