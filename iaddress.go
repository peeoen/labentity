package labentity

type IAddress interface {
	GetAddress() string
	GetPostCode() string
}
