package labentity

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Employee struct {
	Name       string
	LastName   string
	Age        int
	Position   string
	Department string
	Salary     int
	Address    []Address
}

func (e Employee) String() string {
	if rawJson, err := json.MarshalIndent(&e, " ", " "); err != nil {
		return ""
	} else {
		return string(rawJson)
	}
}

func (e *Employee) GetFullName() string {
	return fmt.Sprintf("%s %s",
		e.Name,
		e.LastName)
}

func (e *Employee) GetDepartment() string {
	return fmt.Sprintf("%s",
		e.Department)
}

func FilterEmployee(keyword string, emp Employee) bool {
	return (strings.Contains(strings.ToLower(emp.Name), strings.ToLower(keyword)) ||
		strings.Contains(strings.ToLower(emp.LastName), strings.ToLower(keyword)) ||
		strings.Contains(strings.ToLower(emp.Position), strings.ToLower(keyword)))
}

func SearchEmployee(keyword, department string, emps []Employee) {
	var empFilterd []Employee

	for _, emp := range emps {

		if keyword != "" && department == "" {
			if FilterEmployee(keyword, emp) {
				empFilterd = append(empFilterd, emp)
				continue
			}
		} else if department != "" && keyword == "" {
			if strings.ToLower(emp.Department) == strings.ToLower(department) {
				empFilterd = append(empFilterd, emp)
				continue
			}
		} else if keyword != "" && department != "" {
			if FilterEmployee(keyword, emp) &&
				strings.ToLower(emp.Department) == strings.ToLower(department) {
				empFilterd = append(empFilterd, emp)
				continue
			}
		}
	}

	fmt.Printf("Found %d employees. \n", len(empFilterd))

	for _, emp := range empFilterd {
		fmt.Printf("Name: %s ,LastName: %s , Department: %s \n", emp.Name, emp.LastName, emp.Department)
	}

	// fmt.Printf("%v \n", emps)

}

func FilterEmp(vs []string, f func(string) bool) []string {
	filtered := make([]string, 0)
	for _, v := range vs {
		if f(v) {
			filtered = append(filtered, v)
		}
	}
	return filtered
}

func AddEmployee() (Employee, error) {
	var name, lastname, position, department string
	var age, salary int
	fmt.Printf("--- Register Employee --- \n")

	fmt.Printf("Name: ")
	fmt.Scanf("%s", &name)
	fmt.Scanln()

	fmt.Printf("LastName: ")
	fmt.Scanf("%s", &lastname)
	fmt.Scanln()

	fmt.Printf("Age: ")
	fmt.Scanf("%d", &age)
	fmt.Scanln()

	fmt.Printf("Position: ")
	fmt.Scanf("%s", &position)
	fmt.Scanln()

	fmt.Printf("Department: ")
	fmt.Scanf("%s", &department)
	fmt.Scanln()

	fmt.Printf("Salary: ")
	fmt.Scanf("%d", &salary)
	fmt.Scanln()

	emp := Employee{
		Name:       name,
		LastName:   lastname,
		Age:        age,
		Position:   position,
		Department: department,
		Salary:     salary,
	}

	if result, err := AddAddress(); err != nil {
		fmt.Printf("%s \n", err)
	} else {
		emp.Address = result
	}

	return emp, nil
}

func AddAddress() ([]Address, error) {
	done := false

	var address []Address

	for done == false {

		var addmore string
		var address1, address2, province, country string
		var postcode int

		fmt.Printf("Address1: ")
		fmt.Scanf("%s", &address1)
		fmt.Scanln()

		fmt.Printf("Address2: ")
		fmt.Scanf("%s", &address2)
		fmt.Scanln()

		fmt.Printf("Province: ")
		fmt.Scanf("%s", &province)
		fmt.Scanln()

		fmt.Printf("Country: ")
		fmt.Scanf("%s", &country)
		fmt.Scanln()

		fmt.Printf("Postcode: ")
		fmt.Scanf("%d", &postcode)
		fmt.Scanln()

		address = append(address, Address{
			Address1: address1,
			Address2: address2,
			Province: province,
			Country:  country,
			Postcode: postcode,
		})

		fmt.Printf("Do you want add more address? (Y/N)")
		fmt.Scanf("%s", &addmore)
		fmt.Scanln()
		if strings.ToLower(addmore) == "n" {
			done = true
		}

	}

	return address, nil
}
